<?php

/**
 * Implements hook_entity_translation_insert().
 */
function taxonomy_menu_node_links_entity_translation_insert($entity_type, $entity, $translation, $values = array()) {
  if ($entity_type !== 'node' || $entity->type !== 'page') {
    return;
  }
  taxonomy_menu_node_links_upsert($entity, $translation['language']);
}

/**
 * Implements hook_entity_translation_update().
 */
function taxonomy_menu_node_links_entity_translation_update($entity_type, $entity, $translation, $values = array()) {
  if ($entity_type !== 'node' || $entity->type !== 'page') {
    return;
  }
  taxonomy_menu_node_links_upsert($entity, $translation['language']);
}

/**
 * Implements hook_entity_translation_delete().
 */
function taxonomy_menu_node_links_entity_translation_delete($entity_type, $entity, $langcode) {
  if ($entity_type !== 'node' || $entity->type !== 'page') {
    return;
  }
  // Delete all menu links associated with this translation.
  $menu_links = taxonomy_menu_node_links_load_node_links($entity->nid, $langcode);
  foreach ($menu_links as $menu_link) {
    menu_link_delete($menu_link['mlid']);
  }
}

/**
 * Insert or update taxonomy menu sublinks for a node.
 */
function taxonomy_menu_node_links_upsert($node, $langcode = LANGUAGE_NONE) {
  // Load existing menu links to node created by this module.
  // All menu links point to node.
  $menu_links = taxonomy_menu_node_links_load_node_links($node->nid, $langcode);

  // List of expected term items to have link to this node.
  $selected_tids = !empty($node->field_sections[LANGUAGE_NONE])
    ? array_column($node->field_sections[LANGUAGE_NONE], 'tid')
    : array();

  // Menu link ids expected to have this node as a sublink.
  $taxonomy_menu_mlids = taxonomy_menu_node_links_get_tid_mlids($selected_tids, $langcode);

  // Parent menu items should be links to taxonomy terms.
  $plids = array_column($menu_links, 'plid', 'mlid');

  // Map parent menu item to corresponding term id.
  $tid_map = taxonomy_menu_node_links_map_mlids_to_tids($plids);

  // Update existing links, remove orphans.
  foreach ($menu_links as $menu_link) {
    // node -> node menu link -> parent menu term link -> taxonomy_menu term
    $plid = $menu_link['plid'];
    if (isset($tid_map[$plid])) {
      // Parent item is created by taxonomy_menu.
      $tid = $tid_map[$plid];

      // This term is not selected on node any more. Delete.
      if (!in_array($tid, $selected_tids)) {
        menu_link_delete($menu_link['mlid']);
      }
      elseif ($menu_link['link_title'] !== $node->title_field[$langcode][0]['value']) {
        // Update title and save.
        $updated = array(
          'link_title' => $node->title_field[$langcode][0]['value'],
        ) + $menu_link;
        menu_link_save($updated);
      }
    }
  }

  // Missing links to add.
  if ($missing_link_parents = array_diff($taxonomy_menu_mlids, $plids)) {
    $base_link = array(
      'link_path' => 'node/' . $node->nid,
      'link_title' => $node->title_field[$langcode][0]['value'],
      'menu_name' => 'main-menu',
      'expanded' => TRUE,
      'router_path' => 'node/%',
      'module' => 'taxonomy_menu_node_links',
      'language' => $langcode,
    );  
    foreach ($missing_link_parents as $plid) {
      // Find taxonomy menu items for selected terms.
      $menu_link = array(
        'plid' => $plid
      ) + $base_link;
      menu_link_save($menu_link);
    }
  }
}

/**
 * Get map from menu link ids to term id.
 */
function taxonomy_menu_node_links_map_mlids_to_tids($mlids, $langcode = NULL) {
  if (empty($mlids)) {
    return array();
  }

  $query = db_select('taxonomy_menu','t')
    ->fields('t', array('mlid', 'tid'))
    ->condition('mlid', $mlids, 'IN');

  if ($langcode) {
    $query->condition('language', $langcode);
  }

  return $query->execute()->fetchAllKeyed();
}

/**
 * Get map from term ids to corresponding menu item id.
 */
function taxonomy_menu_node_links_get_tid_mlids($tids, $langcode = NULL) {
  if (empty($tids)) {
    return array();
  }

  $query = db_select('taxonomy_menu','t')
    ->fields('t', array('tid', 'mlid'))
    ->condition('tid', $tids, 'IN');

  if ($langcode) {
    $query->condition('language', $langcode);
  }

  return $query->execute()->fetchAllKeyed();
}

/**
 * Load existing links to nid created by this module.
 */
function taxonomy_menu_node_links_load_node_links($nid, $langcode = NULL) {
  $query = db_select('menu_links', 'ml', array(
    'fetch' => PDO::FETCH_ASSOC,
  ))
    ->fields('ml')
    ->condition('module', 'taxonomy_menu_node_links')
    ->condition('link_path', 'node/' . $nid);

  if ($langcode) {
    $query->condition('language', $langcode);
  }

  $links = $query->execute()->fetchAll();
  foreach ($links as &$link) {
    $link['options'] = unserialize($link['options']);
  }
  return $links;
}
